# Korkey's Dapps

## Huh?
Korkey's dapps is the home of the dapps by Korkey (that's me!).  
It's used by me to keep track of all the stuff I make. And also for you!  

-----

## So why do I care?
I try to build things that are useful for everyone (not just me).  
Like a thing? Cool! Consider donating so I don't get burnt out @ `korkey.eth`  

## Have an idea?
Submit it through issues on gitlab. Maybe it's a good one. Maybe I do it. 🤷‍♂️